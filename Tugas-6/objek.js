var Tahun = new Date().getFullYear();

function arrayToObject(arr) {
    var objek = arr.length;
    for(var i=0;i<objek;i++){
        var data_obj = {
            firstname : arr[i][0],
            lastname : arr[i][1],
            gender : arr[i][2],
            age : Tahun - arr[i][3]
        } 

        if(data_obj.age < 0 || data_obj.age == NaN){
            data_obj.age = "Invalid Birth Year";
        }

        console.log(data_obj);

    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);