function readBook(time,book){
    console.log(`sala mulai membaca ${book[0].name}`)
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            let sisaWaktu = time - book[0].timeSpent
            if(sisaWaktu >= 0){
                console.log(`saya sudah selesai membaca ${book[0].name}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            }else{
                console.log(`saya tidak punya waktu u/ membaca ${book[0].name}`)
                reject(sisaWaktu)
            }

        }, book[0].timeSpent)

    })
}

module.exports = readBook