class Animal {
    // Code class di sini
    constructor(name) {
        this.AnimalName = name;
        this.Legs = 4;
        this.cold_blooded = false;

      }
    get name() {
      return this.AnimalName; 
    }
    get legs() {
        return this.Legs; 
       }
    // set legs(x){
    //     this.Legs = x; 
    // }
    // get cold_blooded() {
    //     return this.cold_blooded; 
    //   }
    //   set cold_blooded(y){
    //     this.cold_blooded = y; 
    // }
}
 
var sheep = new Animal("shaun");
//sheep.legs = 4;
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false