import { styleSheets } from 'min-document';
import React,{useEffect} from 'react'
import { View, Text,Image,StyleSheet,SafeAreaView,FlatList,TouchableOpacity } from 'react-native'
import { check } from 'yargs';
import {Data} from './dummy';


export default function Telegram() {
    useEffect(()=>{
        console.log(Data)
    },[])

    return(
        <View>
            <View>
                <View>
                    <View>
                        <Image
                            style={styles.avatarDrawwer}
                            source={require('./asset/drawwer.png')}
                        />
                        <Text style={styleSheets.titleName}>Telegram</Text>
                    </View>
                    <View>
                       <Image style={styles.avatarDrawwer}source={require('./asset/search.png')}
                        />
                    </View>
                </View>
            </View>
            <View style={styles.content}>
                <SafeAreaView>
                    <FlatList 
                            data={Data}
                            keyExtractor={(item)=>item.id}
                            renderItem={({item})=>{
                                return(
                                    <>
                                        <>
                                        <TouchableOpacity style={styles.messageContainer}>
                                            <View style={{flexDiractions:'row'}}>
                                                <Image style={{height:50,width:50,borderRadius:25}}
                                                source={item.image}/>
                                            <View style={styles.nameMessage}>
                                                <Text>{item.name}</Text>
                                                <Text>{item.message}</Text>
                                            </View>
                                        <View>
                                            <View>
                                                <Image source={require('./asset/check.png')}/>
                                                <Text>{item.time}</Text>
                                            </View>
                                            <View>
                                                <Text>{item.totalMessage}</Text>
                                            </View>
                                        </View>
                                        </TouchableOpacity>
                                        <View>
                                            
                                        </View>
                                        </>
                                )
                            }}
                        />
                </SafeAreaView>
                <TouchableOpacity>
                    <View>
                        <Image source={require('./asset/pencil.png')}/>
                    </View>
                </TouchableOpacity>
        </View>
    )
}
